# Steps to Onboard a New Developer

## Various Access Request Forms to Submit

1. A **NetID** is an automatic part of the onboarding process and submitted by Sherry.  

2. A **key** is an eCat. Info here: <https://facilities.utdallas.edu/plant/#keys>  

3. If student developer, you’ll need to physically add them to administrators on the physical lab computer so that they can install needed apps.  

4. A **CometCard** is an eCat. Info here: <https://cometcard.utdallas.edu/>  

5. **GitLab** JSOMDEV developer access is requested by user, or granted by group owner.  

6. Add to **AD JSOM WEB VPN** group is an eCat:  
    [eCat](https://ecat.utdallas.edu/) > Access Request > Computer Access > VPN >
    - In the _‘List NetIDs and VPN group’_ field, add the user netID and “jsom web vpn”  

7. **Database** access is an Atlas Ticket with the OIT DBA team. You will need to be on the VPN and in the ‘jsom web vpn' group to access the databases, see number 5 above for access.  
    Info here: <https://atlas.utdallas.edu/TDClient/30/Portal/Requests/ServiceDet?ID=122>  

8. **OneDrive database backups** folder access for local development is granted by the owner.  

9. **Linux system file** access is through an eCat ticket:  
    [eCat](https://ecat.utdallas.edu/) > Access Request > Computer Access > Special Drives and/or Folders >
    - Check the ‘Read and Write’ field
    - In _‘List Drives and/or Folders and type of access’_ field write:

        > Needs read/write access to:  
        > \- wwwdav.utdallas.edu/myjsom-apps  
        > \- wwwdav-dev.utdallas.edu/myjsom-apps  
        > \- wwwdav.utdallas.edu/jsom-apps  
        > \- wwwdav-dev.utdallas.edu/myjsom-apps  
        > <br/>As well as highest available ssh access to applicable directories on:  
        > \- aisphpapp01 and  
        > \- aisphpapp02

1. **Splunk** access is through an eCat ticket:  
    [eCat](https://ecat.utdallas.edu/) > Access Request > Computer Access > Security Group Membership (AD Group) >
    - _In ‘Justification for this Request_ field write:  
        “New Staff developer needs access to Splunk server logs”
    - In ‘_List Security Group Membership’_ field write:  
        “UTDJSOMSplunkUsers”
    - Completion of eCat should open an Atlas ticket with ‘OIT TEI Service Desk’ service.  

2. **Atlas JSOMDEV** access is an Atlas ticket:  
    <https://atlas.utdallas.edu/TDClient/30/Portal/Requests/ServiceDet?ID=1>  

3. Add to the applicable **Teams channels**. Info here: <https://support.microsoft.com/en-us/office/add-members-to-a-team-in-microsoft-teams-aff2249d-b456-4bc3-81e7-52327b6b38e9>  
    - JSOM – Digital Services & Analytics
    - JSOM – DS&A Students
    - UTD Web Developers  

4. Add to **<jsomweb@utdallas.edu>** email distribution list is an eCat:  
    <https://atlas.utdallas.edu/TDClient/30/Portal/Requests/ServiceDet?ID=26>

## Tools needed for development

1. IDE (ex: VSCode) <https://code.visualstudio.com/download/>
2. GIT <https://git-scm.com/downloads/>
3. Lando <https://lando.dev/>
4. MySQL Workbench <https://dev.mysql.com/downloads/workbench/>
5. MS Teams [https://atlas.utdallas.edu/TDClient/.../ServiceDet?ID=38](https://atlas.utdallas.edu/TDClient/30/Portal/Requests/ServiceDet?ID=38)
6. Global Protect VPN app <https://utdvpn.utdallas.edu/>
7. BOX <https://atlas.utdallas.edu/TDClient/30/Portal/KB/ArticleDet?ID=824>
8. OneDrive [https://atlas.utdallas.edu/TDClient/.../ServiceDet?ID=15](https://atlas.utdallas.edu/TDClient/30/Portal/Requests/ServiceDet?ID=15)
9. CrashPlan [https://atlas.utdallas.edu/TDClient/.../ArticleDet?ID=1033](https://atlas.utdallas.edu/TDClient/30/Portal/KB/ArticleDet?ID=1033)

## Stuff to configure

1. Upload a SSH key to GitLab