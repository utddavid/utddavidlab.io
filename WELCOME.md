# 😀 Welcome

Welcome to the Internal Developer Documentation for the UT-Dallas Dev Team. These docs will help you become familiar with our codebase and gain a better understanding of our system architecture.

![UT Dallas JSOM Web Services Team](images/utdallasLogo.png "UT Dallas Logo")

If you find any errors or would like to improve the docs, please let your manager know and submit your proposed changes and/or enhancements.